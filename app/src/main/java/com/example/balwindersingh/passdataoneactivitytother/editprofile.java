package com.example.balwindersingh.passdataoneactivitytother;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class editprofile extends AppCompatActivity {
    boolean isupdate = false;
    EditText editemail,editname,editpassword,editphonnumber;
    DatabaseHelper mydb;
    sessionclass sessionclass;
    int id;
    Spinner spinner;
    RadioGroup radioGroup;
    String text;
   RadioButton maleradio,femaleradio;
    RadioButton radioButtons;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editprofile);
    String email,password;
       editemail = (EditText) findViewById(R.id.email) ;
        editname = (EditText) findViewById(R.id.name);
        editpassword = (EditText) findViewById(R.id.password);
        editphonnumber = (EditText) findViewById(R.id.contact);
       maleradio = (RadioButton) findViewById(R.id.male);
       femaleradio = (RadioButton) findViewById(R.id.female);

        //creating spiiner items
        ArrayAdapter<String> myadapter = new ArrayAdapter<>(editprofile.this,
                android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.names));
        spinner = findViewById(R.id.spinner);
        myadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(myadapter);



        Intent intent = getIntent();
        email = intent.getStringExtra("email");
        password = intent.getStringExtra("name");
        mydb = new DatabaseHelper(this);
        sessionclass = new sessionclass(this);
        Cursor data = mydb.getListContentsSingle(email, password);

        if (data.getCount() == 0) {
            Toast.makeText(getApplicationContext(), "conection lost", Toast.LENGTH_LONG).show();
            //Toast.makeText(getApplicationContext(), sessionclass.getName(), Toast.LENGTH_LONG).show();
            Intent myIntent = new Intent(editprofile.this, MainActivity.class);
            startActivity(myIntent);
        }
        else {
            // Toast.makeText(getApplicationContext(), sessionclass.getName(), Toast.LENGTH_LONG).show();
            data.moveToFirst();
            id= data.getInt(0);
            editname.setText(data.getString(1));
            editphonnumber.setText(data.getString(3));

            editemail.setText(data.getString(4));
            editpassword.setText(data.getString(6));
           // spinner.setOnItemSelectedListener(this);
            String mystring = data.getString(2);
            int spinerposion = myadapter.getPosition(mystring);
            spinner.setSelection(spinerposion);


//radio button
             String  gender = data.getString(5);


          if(gender.equals("male")){
                maleradio.setChecked(true);
            }else{
                femaleradio.setChecked(true);
            }


        }
        radioGroup = findViewById(R.id.radiogroup);



    }
    public void radiogroup(View v){
        int radiobuttonid = radioGroup.getCheckedRadioButtonId();
        radioButtons =  findViewById(radiobuttonid);
        text = (String) radioButtons.getText();
        Toast.makeText(getBaseContext(),radioButtons.getText(),Toast.LENGTH_LONG).show();
    }




    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.changeprofiledetail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void change(MenuItem item) {
        sessionclass sessionclass = new sessionclass(editprofile.this);
        mydb = new DatabaseHelper(this);
        String email = editemail.getText().toString();
        String name = editname.getText().toString();
        String phonnumber = editphonnumber.getText().toString();
        String password = editpassword.getText().toString();
        String bloodgroup = spinner.getSelectedItem().toString();

        Cursor data = mydb.getListContentsSingle1(email);
        sessionclass sessionclass1 = new sessionclass(this);
//        if (data.getCount() == 0) {
//            Toast.makeText(editprofile.this, "not data  found ", Toast.LENGTH_LONG).show();
//
//        } else {
        // String id = data.getString(0);
        isupdate = mydb.updatedata(id, email, name, phonnumber, password, bloodgroup,text);


        if (isupdate == true) {
            // Toast.makeText(signup.this, "data is inserted ", Toast.LENGTH_LONG).show();


            if (sessionclass1.getName().equals(email)) {
                Toast.makeText(getApplicationContext(), "email upgraded", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(editprofile.this, loginuserdetail.class);
                intent.putExtra("Name", email);
                intent.putExtra("password", password);
                startActivity(intent);
            } else {
                sessionclass1.removeuser();
                Intent intent = new Intent(editprofile.this, MainActivity.class);
                startActivity(intent);

            }
        } else {

        }


        // Intent myIntent = new Intent(editprofile.this, loginuserdetail.class);

        // myIntent.putExtra("userdata ",sessionclass.getName());
        //  myIntent.putExtra("userpass ",sessionclass.getPass());

        // this.startActivity(myIntent);
        //  finish();

    }
    }


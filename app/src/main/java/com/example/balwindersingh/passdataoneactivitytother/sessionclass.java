package com.example.balwindersingh.passdataoneactivitytother;

import android.app.backup.SharedPreferencesBackupHelper;
import android.content.Context;
import android.content.SharedPreferences;

public class sessionclass {


   SharedPreferences sharedPreferences;

   private String name;
    private String pass;
    Context context;


    public String getPass() {
        pass = sharedPreferences.getString("password","");
        return pass;
    }

    public void setPass(String pass) {
        sharedPreferences.edit().putString("password",pass).commit();
        this.pass = pass;
    }



   public void removeuser(){
       sharedPreferences.edit().clear().commit();
   }

    public void setName(String name) {


        sharedPreferences.edit().putString("Name",name).commit();
        this.name = name;

    }

    public String getName() {
     name = sharedPreferences.getString("Name","");
        return name;
    }




   public sessionclass(Context context){

       sharedPreferences = context.getSharedPreferences("userinfo",Context.MODE_PRIVATE);
       this.context=context;

   }
}

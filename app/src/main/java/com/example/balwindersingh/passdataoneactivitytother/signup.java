package com.example.balwindersingh.passdataoneactivitytother;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class signup extends AppCompatActivity {
    boolean isInserted = false;
    DatabaseHelper mydb;
    RadioGroup radioGroup;
    RadioButton radioButtons;
    String bloodgroup;
    Spinner spinner;
    Button btn;
    EditText name1,phonenumber1,userid1,password1;
    Cursor count;
  String text;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        btn = findViewById(R.id.create_account);
        mydb = new DatabaseHelper(this);
        ArrayAdapter<String> myadapter = new ArrayAdapter<>(signup.this,
                                android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.names));
        spinner = findViewById(R.id.spinner);
        myadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(myadapter);


        radioGroup = findViewById(R.id.rgroup);
        name1 = findViewById(R.id.name);
        phonenumber1 = findViewById(R.id.phonenumber);
        userid1 = findViewById(R.id.user_id);
        password1 = findViewById(R.id.password);
        addUser();
        movefocus();

   checkmail();

    }

    public static boolean isValidEmailAddress(String emailAddress) {
        String emailRegEx;
        Pattern pattern;
        // Regex for a valid email address
        emailRegEx = "^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$";
        // Compare the regex with the email address
        pattern = Pattern.compile(emailRegEx);
        Matcher matcher = pattern.matcher(emailAddress);
        if (!matcher.find()) {
            return false;
        }
        return true;
    }






    public void radiogroup(View v){
        int radiobuttonid = radioGroup.getCheckedRadioButtonId();
        radioButtons =  findViewById(radiobuttonid);
         text = (String) radioButtons.getText();
        Toast.makeText(getBaseContext(),radioButtons.getText(),Toast.LENGTH_LONG).show();
    }
  public void movefocus(){
      password1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
          @Override
          public void onFocusChange(View v, boolean hasFocus) {
              if(password1.getText().length()>5 && password1.getText().length()<10){
                  btn.setEnabled(true);

              }else if(password1.getText().length()<=5 || password1.getText().length()>=10){
                  btn.setEnabled(false);
                  password1.setError("password lengh should between 6 to 10");
              }


          }
      });
  }

    public void checkmail(){
        userid1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                 count = mydb.checkemail(userid1.getText().toString());
                if(count.getCount()>=1) {
                    userid1.setError("email already used");
                    Toast.makeText(signup.this,"email already used",Toast.LENGTH_LONG).show();
                }
            }
        });
    }



    public void addUser(){
        btn.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if(name1.getText().length()==0){
                            Toast.makeText(signup.this,"enter the name",Toast.LENGTH_SHORT).show();
                            name1.requestFocus();
                        } else if(userid1.getText().length()==0 || count.getCount()>=1){
                            Toast.makeText(signup.this,"enter the email",Toast.LENGTH_SHORT).show();
                            userid1.requestFocus();
                        }
//                        else if(count.getCount()>=1){
//                            Toast.makeText(signup.this,"this email addres is already usaed ",Toast.LENGTH_LONG).show();
//                            userid1.requestFocus();
//                        }
                        else if(password1.getText().length()==0){
                            Toast.makeText(signup.this,"enter the password",Toast.LENGTH_LONG).show();
                            password1.requestFocus();

                        }


                        else if(phonenumber1.getText().length()<10 || phonenumber1.getText().length()>10){

                            Toast.makeText(signup.this,"enter valid phone number",Toast.LENGTH_LONG).show();
                            phonenumber1.requestFocus();
                            phonenumber1.setError("entr valid phon number");
                        }

                        else if(text==null){

                            Toast.makeText(signup.this, " select Gender ", Toast.LENGTH_LONG).show();

                        }

                        else if(spinner.getSelectedItem().toString().equals("choose")) {
                            Toast.makeText(signup.this, "choose a blood group ", Toast.LENGTH_LONG).show();
                            spinner.requestFocus();
                        }else if( isValidEmailAddress(userid1.getText().toString()) == false) {
                            Toast.makeText(signup.this, "enter valid email id  ", Toast.LENGTH_LONG).show();
                               userid1.requestFocus();
                        }




                        else {
                            bloodgroup = spinner.getSelectedItem().toString();
                            Toast.makeText(getApplicationContext(), bloodgroup, Toast.LENGTH_SHORT).show();

                             isInserted = mydb.insertdata(name1.getText().toString(), bloodgroup,
                                    phonenumber1.getText().toString(),
                                    userid1.getText().toString(),
                                    text,
                                    password1.getText().toString()



                            );
                            Intent myIntent = new Intent(signup.this, signup.class);
                            startActivity(myIntent);

                        }

                            //  String name,String bloodgroup,String phonenumber,String email,String gender,String password
                            if (isInserted == true) {
                                Toast.makeText(signup.this, "data is inserted ", Toast.LENGTH_LONG).show();
                                name1.setText("");
                                phonenumber1.setText("");
                                userid1.setText("");
                                password1.setText("");
                                radioGroup.clearCheck();
                                //radioButtons.toggle();
                                spinner.setSelection(0);

                            Intent intent = new Intent(signup.this,MainActivity.class);
                            startActivity(intent);
                            }


                    else {
                                 Toast.makeText(signup.this, "data not inserted ", Toast.LENGTH_LONG).show();
                            }
                    }
                }
//                        text = spinner.getSelectedItem().toString();
//                        Toast.makeText(getApplicationContext(),text,Toast.LENGTH_SHORT).show();



        );
    }





}
package com.example.balwindersingh.passdataoneactivitytother;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Timer timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                sessionclass sessionclass = new sessionclass(splash.this);
                if(sessionclass.getName()!="") {

                    Intent intent = new Intent(splash.this, loginuserdetail.class);

                    intent.putExtra("Name",sessionclass.getName());
                    intent.putExtra("password",sessionclass.getPass());
                  // Toast.makeText(getApplicationContext(),sessionclass.getName(),S)
                    //intent.putExtra("pass",sessionclass.getPass());

                    startActivity(intent);
                    finish();
                }else {
                    startActivity(new Intent(splash.this,MainActivity.class));
                    finish();
                }
            }
        },1000);
    }
}

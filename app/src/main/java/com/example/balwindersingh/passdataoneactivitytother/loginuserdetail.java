package com.example.balwindersingh.passdataoneactivitytother;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class loginuserdetail extends AppCompatActivity {
    sessionclass sessionclass;
    DatabaseHelper mydb;
    String passwordnew;
    TextView showemail,showname,showphonenumber,showgender,showbloodgroup;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginuserdetail);

        String Name, password;
        Intent intent = getIntent();
        Name = intent.getStringExtra("Name");
        password = intent.getStringExtra("password");
        Toast.makeText(getApplicationContext(), Name, Toast.LENGTH_LONG).show();
        mydb = new DatabaseHelper(this);
        sessionclass = new sessionclass(this);
        Cursor data = mydb.getListContentsSingle(Name, password);
            if (data.getCount() == 0) {
            Toast.makeText(getApplicationContext(), "enter valid email", Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), sessionclass.getName(), Toast.LENGTH_LONG).show();
            Intent myIntent = new Intent(loginuserdetail.this, MainActivity.class);
           startActivity(myIntent);
        } else {
               // Toast.makeText(getApplicationContext(), sessionclass.getName(), Toast.LENGTH_LONG).show();
            data.moveToFirst();
            showname =(TextView) findViewById(R.id.name);
            showemail = (TextView) findViewById(R.id.email);
            showgender =(TextView) findViewById(R.id.gender);
            showphonenumber = (TextView) findViewById(R.id.phonenumber);
            showbloodgroup = (TextView) findViewById(R.id.bloodgroup);
            showname.setText(data.getString(1));
            showphonenumber.setText(data.getString(3));
            showgender.setText(data.getString(5));
            showbloodgroup.setText(data.getString(2));
            showemail.setText(data.getString(4));
              passwordnew=  data.getString(6);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.editprofile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void editprofile(MenuItem item) {
        Intent myIntent = new Intent(this, editprofile.class);
       String email =  showemail.getText().toString();

        sessionclass = new sessionclass(this);
        myIntent.putExtra("email", email);
        myIntent.putExtra("name", passwordnew);
        this.startActivity(myIntent);


    }
    public void logout(View  view){
         new sessionclass(loginuserdetail.this).removeuser();
         Intent intent=new Intent(loginuserdetail.this,MainActivity.class);
         startActivity(intent);
         finish();
    }
}

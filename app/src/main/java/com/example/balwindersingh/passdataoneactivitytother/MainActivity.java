package com.example.balwindersingh.passdataoneactivitytother;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

DatabaseHelper mydb;


      EditText username,password;
      Button Login,Signup,catagories;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mydb = new DatabaseHelper(this);
        username = findViewById(R.id.user);
        password = findViewById(R.id.password);
        Login = findViewById(R.id.login);
        Signup = findViewById(R.id.signup);
        catagories = findViewById(R.id.Direct_Access_Blood);


        addUser();
        showbloodcatagories();
        loginnow();
//             sessionclass sessionclass = new sessionclass(MainActivity.this) ;
////       if(sessionclass.getName()!=""){
////        Intent intent = new Intent(MainActivity.this,loginuserdetail.class);
////           startActivity(intent);
////           //finish();
////       }
////       else{
////           Intent intent = new Intent(MainActivity.this,MainActivity.class);
////           startActivity(intent);
////        // finish();
////       }


    }
    public void addUser(){
        Signup.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent myIntent = new Intent(MainActivity.this, signup.class);

                    startActivity(myIntent);
                    }
                }
        );
    }


    public void showbloodcatagories(){
        catagories.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent myIntent = new Intent(MainActivity.this, singlecatagory.class);

                        startActivity(myIntent);
                    }
                }
        );
    }



    public void loginnow() {



            Login.setOnClickListener(

                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (username.getText().length() == 0 && password.getText().length() == 0) {
                                Toast.makeText(getApplicationContext(), "enter email", Toast.LENGTH_LONG).show();
                            }
                            else {

                                //string session
                                String email = username.getText().toString();
                                String password1 = password.getText().toString();


                                sessionclass sessionclass = new sessionclass(MainActivity.this) ;
                               sessionclass.setName(email);
                                sessionclass.setPass(password1);

                               // Toast.makeText(getApplicationContext(), sessionclass.getName(), Toast.LENGTH_LONG).show();
                                Intent myIntent = new Intent(MainActivity.this, loginuserdetail.class);
                                myIntent.putExtra("Name",  sessionclass.getName());
                                myIntent.putExtra("password", sessionclass.getPass());
                                startActivity(myIntent);
                            }
                        }
                    }
            );
        }

}

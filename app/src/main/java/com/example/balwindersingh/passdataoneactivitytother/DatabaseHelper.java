package com.example.balwindersingh.passdataoneactivitytother;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.opengl.GLES32;
import android.os.Debug;
import android.widget.EditText;

import org.w3c.dom.Text;

import java.sql.Blob;
import java.sql.Date;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Blood.db";
    public static final String TABLE_NAME = "blood_table";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "NAME";
    public static final String COL_3 = "BLOOD_GROUP";
    public static final String COL_4 = "PHONE_NUMBER";
    public static final  String COL_5 = "EMAIL";
    public static final  String COL_6 = "GENDER";
    public static final  String COL_7 = "PASSWORD";
    //public static final  String Col_6 = "lattitude";
    //public static final  String Col_7 = "longitude";




    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);

        SQLiteDatabase db = this.getWritableDatabase();


    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+TABLE_NAME +"(ID INTEGER PRIMARY KEY AUTOINCREMENT ,NAME TEXT,BLOOD_GROUP TEXT,PHONE_NUMBER INTEGER,EMAIL TEXT,GENDER TEXT,PASSWORD TEXT)");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);


    }


    public boolean insertdata(String name,String bloodgroup,String phonenumber,String email,String gender,String password){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues  contentValues = new ContentValues();
        contentValues.put(COL_2,name);
        contentValues.put(COL_3,bloodgroup);
        contentValues.put(COL_4,phonenumber);
        contentValues.put(COL_5,email);
        contentValues.put(COL_6,gender);
        contentValues.put(COL_7,password);
        //contentValues.put(Col_6,lattitude);
        //contentValues.put(Col_7,longitude);
        //contentValues.put(Col_6, date);

        long result = db.insert(TABLE_NAME,null,contentValues);

        if(result == -1)
            return false;
        else
            return true;
    }


    public Cursor getListContentsSingle(String email,String password){
        SQLiteDatabase db = this.getWritableDatabase();

        //  return  data;
        return db.rawQuery("SELECT * FROM "+TABLE_NAME + " Where " + COL_5 + " = '" + email + "'" + " AND "  + COL_7 +  " = '" + password + "'" ,null);
    }
    public Cursor getListContentsSingle1(String email){
        SQLiteDatabase db = this.getWritableDatabase();

        //  return  data;
        return db.rawQuery("SELECT * FROM "+TABLE_NAME + " Where " + COL_5 + " = '" + email + "'"  ,null);
    }


    public Cursor getListContents(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("SELECT * FROM "+TABLE_NAME,null);
        return  data;

    }
    public boolean updatedata(int id,String email,String name,String phonnumber,String password,String bloodgroup,String gender) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, name);
         contentValues.put(COL_3,bloodgroup);
        contentValues.put(COL_4, phonnumber);
        contentValues.put(COL_5, email);
       contentValues.put(COL_6,gender);
        contentValues.put(COL_7, password);

            long result = db.update(TABLE_NAME, contentValues, COL_1 + "=" + id, null);

            if (result == -1)
                return false;
            else {
                return true;
            }

    }
    public Cursor checkemail(String email){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("SELECT * FROM "+TABLE_NAME + " Where " + COL_5 + " = '" + email + "'"  ,null);
        return  data;

    }

    }


